import styled from "styled-components";
import CardNumberSelector from "../components/CardNumberSelector";
import StartNewGameButton from "../components/StartNewGameButton";
import { useState } from "react";

const Wrapper = styled.div`
  display: flex;
  margin: auto;
  align-items: center;
  justify-content: center;
`;

const Headertext = styled.h1`
  margin: var(--margin-header__TEXT);
`;
const CardSelectorLabel = styled.label`
  padding: var(--padding-normal);
`;

function Homepage() {
  const [numberOfCardsToPlay, setNumberOfCardsToPlay] = useState(6); //ez mennyire gáz?

  return (
    <>
      <Wrapper>
        <Headertext>Splendex Memory Game</Headertext>
      </Wrapper>
      <Wrapper>
        <CardSelectorLabel>Deck Size</CardSelectorLabel>
      </Wrapper>
      <Wrapper>
        <CardNumberSelector
          selected={numberOfCardsToPlay}
          onChangeSelection={setNumberOfCardsToPlay}
        />
      </Wrapper>
      <Wrapper>
        <StartNewGameButton numberOfCardsToPlay={numberOfCardsToPlay} />
      </Wrapper>
    </>
  );
}
export default Homepage;
