import { Redirect } from "react-router-dom";
import { useContext, useEffect } from "react/cjs/react.development";
import GameContext from "../store/game-context";
import Deck from "../components/Deck";
import styled from "styled-components";
import Victory from "../components/Victory";
import GameHeader from "../components/GameHeader";

const GameArea = styled.div`
  display: flex;
  flex-flow: row wrap;
  align-items: center;
  justify-content: center;
  max-width: var(--play-area-size);
  margin: auto;
`;

function Gamepage() {
  const ctx = useContext(GameContext);

  function restartGameHandler() {
    ctx.onRestartGame(true);
  }

  useEffect(() => {
    if (ctx.gameRestart === true) {
      ctx.onRestartGame(false);
    }
  }, [ctx]);

  return (
    <>
      {!ctx.gameIsRunning && <Redirect to="/home" />}
      <GameArea>
        <GameHeader restartGame={restartGameHandler} />
        {ctx.gameIsWon && <Victory />}
        {!ctx.gameRestart && <Deck />}
      </GameArea>
    </>
  );
}
export default Gamepage;
