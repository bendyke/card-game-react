import { Route, Redirect } from "react-router-dom";
import Homepage from "./pages/Homepage";
import Gamepage from "./pages/Gamepage";
import MainHeader from "./components/MainHeader";

function App() {
  return (
    <div>
      <MainHeader></MainHeader>
      <main>
        <Route path="/" exact>
          <Redirect to="/home" />
        </Route>
        <Route path="/home">
          <Homepage />
        </Route>
        <Route path="/game">
          <Gamepage />
        </Route>
      </main>
    </div>
  );
}

export default App;
