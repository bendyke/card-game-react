import React, { useState } from "react";

const GameContext = React.createContext({
  gameIsRunning: false,
  numberOfCards: null,
  numberOfTries: null,
  gameIsWon: false,
  gameRestart: null,
  onStartGame: (numberOfCards) => {},
  onAddTry: () => {},
  isGameWon: () => {},
  onRestartGame: (newVal) => {},
});

export const GameContextProvider = (props) => {
  const [gameIsRunning, setGameIsRunning] = useState(false);
  const [numberOfCards, setNumberOfCards] = useState("");
  const [numberOfTries, setNumberOfTries] = useState(0);
  const [gameIsWon, setGameIsWon] = useState(false);
  const [gameRestart, setGameRestart] = useState(false);

  function gameStartHandler(numberOfCards) {
    if (!gameIsRunning) {
      setGameIsRunning(true);
      setNumberOfCards(numberOfCards);
      setNumberOfTries(0);
    } else {
      setNumberOfCards(numberOfCards);
      restartGame(true);
    }
  }

  function addTry() {
    setNumberOfTries((prev) => {
      return prev + 1;
    });
  }

  function onGameWon(newVal) {
    setGameIsWon(newVal);
  }

  function restartGame(newVal) {
    setNumberOfTries(0);
    setGameRestart(newVal);
  }

  return (
    <GameContext.Provider
      value={{
        gameIsRunning: gameIsRunning,
        numberOfCards: numberOfCards,
        numberOfTries: numberOfTries,
        gameIsWon: gameIsWon,
        gameRestart: gameRestart,
        onRestartGame: restartGame,
        onStartGame: gameStartHandler,
        onAddTry: addTry,
        onGameWon: onGameWon,
      }}
    >
      {props.children}
    </GameContext.Provider>
  );
};

export default GameContext;
