import styled from "styled-components";

const CardAmountSelector = styled.select`
  height: var(--standard-element-hegiht);
  width: var(--standard-element-width);
  font-size: var(--font-size-large);
  text-align: center;
  border-radius: var(--border-radius-normal);
`;

function CardNumberSelector(props) {
  function changeHandler(event) {
    props.onChangeSelection(event.target.value);
  }

  return (
    <>
      <CardAmountSelector value={props.selected} onChange={changeHandler}>
        <option value={6}>6</option>
        <option value={8}>8</option>
        <option value={10}>10</option>
        <option value={12}>12</option>
        <option value={14}>14</option>
        <option value={16}>16</option>
        <option value={18}>18</option>
        <option value={20}>20</option>
      </CardAmountSelector>
    </>
  );
}

export default CardNumberSelector;
