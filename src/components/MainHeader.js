import { useContext, useState } from "react/cjs/react.development";
import styled from "styled-components";
import Logo from "../icons/splendex-logo.svg";
import GameContext from "../store/game-context";
import CardNumberSelector from "./CardNumberSelector";
import StartNewGameButton from "./StartNewGameButton";

const Header = styled.div`
  display: flex;
  justify-content: space-evenly;
  align-items: center;
  height: var(--header-element-height);
  background-color: black;
  padding: var(--header-padding);
`;

const HeaderLogo = styled.svg`
  position: absolute;
  left: var(--logo-margin-left);
  background-image: url(${Logo});
  height: var(--header-element-height);
  width: var(--header-element-width);
`;

function MainHeader() {
  const ctx = useContext(GameContext);
  const [numberOfCardsToPlay, setNumberOfCardsToPlay] = useState(6);

  return (
    <>
      <Header>
        <HeaderLogo />
        {ctx.gameIsRunning && (
          <div>
            <CardNumberSelector
              selected={numberOfCardsToPlay}
              onChangeSelection={setNumberOfCardsToPlay}
            />
            <StartNewGameButton numberOfCardsToPlay={numberOfCardsToPlay} />
          </div>
        )}
      </Header>
    </>
  );
}

export default MainHeader;
