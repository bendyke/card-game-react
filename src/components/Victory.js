import { useContext } from "react/cjs/react.development";
import styled from "styled-components";
import GameContext from "../store/game-context";

const VictoryHeader = styled.h1`
  width: 100%;
  text-align: center;
  font-size: var(--font-size-very-large);
`;

const VictoryStats = styled.h1`
  width: 100%;
  text-align: center;
  font-size: var(--font-size-large);
`;

function Victory() {
  const ctx = useContext(GameContext);

  return (
    <>
      <VictoryHeader>You Win!</VictoryHeader>
      <VictoryStats>Number of tries: {ctx.numberOfTries}</VictoryStats>
    </>
  );
}

export default Victory;
