import { useContext } from "react/cjs/react.development";
import styled from "styled-components";
import GameContext from "../store/game-context";

const HeaderDiv = styled.div`
  display: flex;
  width: 100%;
  text-align: center;
  justify-content: space-between;
`;

const Tries = styled.h2`
  font-size: var(--font-size-large);
`;

const RestartButton = styled.button`
  height: var(--standard-element-hegiht);
  margin: var(--margin-larger);
  background-color: var(--button-bg-color);
  color: black;
  font-size: var(--font-size-large);
  border-radius: var(--border-radius-normal);
`;

function GameHeader(props) {
  const ctx = useContext(GameContext);

  return (
    <>
      <HeaderDiv>
        <Tries>Current Tries: {ctx.numberOfTries}</Tries>
        <RestartButton onClick={props.restartGame}>Restart Game</RestartButton>
      </HeaderDiv>
    </>
  );
}

export default GameHeader;
