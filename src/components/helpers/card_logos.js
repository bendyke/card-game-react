import logo1 from "../../icons/cards/angular.png";
import logo2 from "../../icons/cards/d3.png";
import logo3 from "../../icons/cards/jenkins.png";
import logo4 from "../../icons/cards/postcss.png";
import logo5 from "../../icons/cards/react.png";
import logo6 from "../../icons/cards/redux.png";
import logo7 from "../../icons/cards/sass.png";
import logo8 from "../../icons/cards/splendex.png";
import logo9 from "../../icons/cards/ts.png";
import logo10 from "../../icons/cards/webpack.png";

const CARD_LOGOS = [
  logo1,
  logo2,
  logo3,
  logo4,
  logo5,
  logo6,
  logo7,
  logo8,
  logo9,
  logo10,
];

export default CARD_LOGOS;
