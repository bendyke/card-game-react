import { useState, useEffect, useContext } from "react/cjs/react.development";
import GameContext from "../store/game-context";
import Card from "./Card";
import CARD_LOGOS from "./helpers/card_logos";
import shuffleArray from "./helpers/array_shuffler";

function Deck(props) {
  const ctx = useContext(GameContext);
  const [deck, setDeck] = useState([]);
  const [flippedStates, setFlippedStates] = useState([]);
  const [foundStates, setFoundStates] = useState([]);
  const [card1ID, setCard1ID] = useState("");
  const [card2ID, setCard2ID] = useState("");

  useEffect(() => {
    for (let i = 0; i < ctx.numberOfCards; i++) {
      setFlippedStates((prevStates) => {
        return [...prevStates, false];
      });
      setFoundStates((prevStates) => {
        return [...prevStates, false];
      });
    }
  }, [ctx.gameRestart, ctx.numberOfCards]);

  useEffect(() => {
    setDeck(() => {
      let cardsTemp = [];
      for (let i = 0; i < ctx.numberOfCards / 2; i++) {
        cardsTemp.push(
          {
            key: Math.random(),
            cardLogo: CARD_LOGOS[i],
          },
          {
            key: Math.random(),
            cardLogo: CARD_LOGOS[i],
          }
        );
      }
      if (ctx.gameRestart) {
        ctx.onRestartGame(false);
      }
      return shuffleArray(cardsTemp);
    });
  }, [ctx.gameRestart]);

  function flipCardHandler(CardID) {
    let cardIdx = deck.findIndex((element) => element.key === CardID);
    let newVal = !flippedStates[cardIdx];
    setFlippedStates((prevStates) => {
      return [
        ...prevStates.slice(0, cardIdx),
        newVal,
        ...prevStates.slice(cardIdx + 1),
      ];
    });
  }

  function foundCardHandler(CardID, newVal) {
    let cardIdx = deck.findIndex((element) => element.key === CardID);
    setFoundStates((prevStates) => {
      return [
        ...prevStates.slice(0, cardIdx),
        newVal,
        ...prevStates.slice(cardIdx + 1),
      ];
    });
  }

  useEffect(() => {
    let card1 = deck.findIndex((card) => card.key === card1ID);
    let card2 = deck.findIndex((card) => card.key === card2ID);
    if (foundStates[card1] === false && foundStates[card2] === false) {
      let card1 = card1ID;
      let card2 = card2ID;
      setCard1ID("");
      setCard2ID("");
      const delay = setTimeout(() => {
        flipCardHandler(card1);
        flipCardHandler(card2);
      }, 700);

      return () => clearTimeout(delay);
    }
    setCard1ID("");
    setCard2ID("");
  }, [foundStates]);

  useEffect(() => {
    if (foundStates.length > 0) {
      let iswon = foundStates.every((element) => element === true);
      ctx.onGameWon(iswon);
    }
  }, [foundStates]);

  function checkCardHandler(CardID) {
    flipCardHandler(CardID);
    if (card1ID === "") {
      setCard1ID(CardID);
    } else {
      setCard2ID(CardID);
      if (card1ID === CardID) {
        flipCardHandler(CardID);
        setCard1ID("");
        setCard2ID("");
      } else {
        ctx.onAddTry();
        let card1 = deck.find((card) => card.key === card1ID);
        let card2 = deck.find((card) => card.key === CardID);

        if (card1.cardLogo === card2.cardLogo) {
          foundCardHandler(card1ID, true);
          foundCardHandler(CardID, true);
        } else {
          foundCardHandler(card1ID, false);
          foundCardHandler(CardID, false);
        }
      }
    }
  }

  return (
    <>
      {deck.map((card, index) => (
        <Card
          key={card.key}
          ID={card.key}
          cardLogo={card.cardLogo}
          isFlipped={flippedStates[index]}
          isFound={foundStates[index]}
          revertCard={flipCardHandler}
          checkCard={checkCardHandler}
        />
      ))}
    </>
  );
}

export default Deck;
