import styled from "styled-components";
import { useContext } from "react";
import { NavLink } from "react-router-dom";
import GameContext from "../store/game-context";

const StartGameButton = styled(NavLink)`
  height: var(--standard-element-hegiht);
  margin: var(--margin-larger);
  line-height: var(--standard-element-hegiht);
  background-color: var(--button-bg-color);
  color: white;
  font-size: var(--font-size-large);
  border-radius: var(--border-radius-normal);
  border-width: var(--card-border-width);
  border-style: solid;
  text-decoration: none;
  padding: var(--padding-normal);
`;

function StartNewGameButton(props) {
  const ctx = useContext(GameContext);

  function clickHandler(event) {
    ctx.onStartGame(props.numberOfCardsToPlay);
  }

  return (
    <>
      <StartGameButton to="/game" onClick={clickHandler}>
        START NEW GAME
      </StartGameButton>
    </>
  );
}

export default StartNewGameButton;
