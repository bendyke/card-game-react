import { useEffect, useState } from "react/cjs/react.development";
import styled from "styled-components";

const StyledCard = styled.div`
  height: var(--card-size);
  width: var(--card-size);
  box-shadow: var(--card-box-shadow);
  border-radius: var(--card-border-radius);
  border-style: ${(props) => (props.isFound ? "none" : "solid")};
  border-width: var(--card-border-width);
  margin: var(--margin-cards);
  background-color: ${(props) =>
    props.isFlipped ? "transparent" : "var(--card-unflipped-color)"};
`;

const CardSvg = styled.svg`
  height: inherit;
  width: inherit;
  background-image: ${(props) => `url(${props.logo})`};
  opacity: ${(props) =>
    props.isFound
      ? "var(--card-found-opacity)"
      : "var(--card-notfound-opacity)"};
`;

const Card = (props) => {
  const [logo, setLogo] = useState(props.logo);
  const [isFlipped, setIsFlipped] = useState(false);
  const [isFound, setIsFound] = useState(false);

  useEffect(() => {
    setLogo(props.cardLogo);
    setIsFlipped(props.isFlipped);
    setIsFound(props.isFound);
  }, [props.cardLogo, props.isFlipped, props.isFound]);

  function flipClickHandler() {
    if (!isFound) {
      props.checkCard(props.ID);
    }
  }

  return (
    <StyledCard
      onClick={flipClickHandler}
      isFlipped={isFlipped}
      isFound={isFound}
    >
      {isFlipped && <CardSvg logo={logo} isFound={isFound} />}
    </StyledCard>
  );
};

export default Card;
